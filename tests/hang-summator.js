var s = new Solver();

var a1 = s.alloc();
var a2 = s.alloc();
var a3 = s.alloc();

s.add_rule(a1, P1, a2, P1, a3, P1.neg());

var b1 = s.alloc();
var b2 = s.alloc();

s.add_rule(b1, S, b2, P1.neg());

var c1 = s.alloc();
var c2 = s.alloc();

s.add_rule(a2, P1, b1, P1.neg());
s.add_rule(b2, P1, a3, P1.neg());
s.add_rule(a1, P1, c1, P1.neg());
s.add_rule(a2, P1, c2, P1.neg());

s.solve();
