import random, json

print('''\
BigNum = require("./bignum.js");

function assert_equal(a, b, s)
{
    var q = a.sub(b);
    if(q.data.length != 1 || q.data[0] != 0)
        throw new Error(s);
}
''')

random.seed(179)

def stringify(n):
    if n >= 0: return '(new BigNum("%s"))'%n
    return '(new BigNum("%s")).neg()'%-n

def generate_one():
    size1 = random.randint(1, 10)
    size2 = random.randint(1, 10)
    a = random.randint(-2**(32*size1-1), 2**(32*size1-1)-1)
    b = random.randint(-2**(32*size2-1), 2**(32*size2-1)-1)
    if b == 0:
        op = random.choice([('add', '', a+b, '+'), ('sub', '', a-b, '-'), ('mul', '', a*b, '*')])
    else:
        op = random.choice([('add', '', a+b, '+'), ('sub', '', a-b, '-'), ('mul', '', a*b, '*'), ('divmod', '[0]', a//b, '//'), ('divmod', '[1]', a%b, '%')])
    method, suffix, ans, sign = op
    expr = '%s.%s(%s)%s'%(stringify(a), method, stringify(b), suffix)
    s = '%d %s %d == %d'%(a, sign, b, ans)
    return 'assert_equal(%s, %s, "%s")'%(expr, stringify(ans), s)

for i in range(1000): print(generate_one())
