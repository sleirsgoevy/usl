function Canvas(container)
{
    this.container = container;
    this.moving = null;
    this.moving_x = 0;
    this.moving_y = 0;
    this.fixed_inputs = [];
    this.observers = [];
    this.container.onmousemove = function(event)
    {
        if(this.moving !== null)
        {
            var x = this.moving_x + event.pageX;
            var y = this.moving_y + event.pageY;
            this.last_x = x;
            this.last_y = y;
            this.moving.move(x, y);
        }
    }.bind(this);
    this.container.onmouseup = function()
    {
        if(this.moving !== null)
            this.resetMovingElement(this.moving);
    }.bind(this);
    this.current_dot = null;
    this.last_x = 0;
    this.last_y = 0;
}

Canvas.prototype.appendChild = function(x)
{
    this.container.appendChild(x);
}

Canvas.prototype.removeChild = function(x)
{
    this.container.removeChild(x);
}

Canvas.prototype.setMovingElement = function(e, dx, dy)
{
    this.moving = e;
    this.moving_x = dx;
    this.moving_y = dy;
}

Canvas.prototype.resetMovingElement = function(e)
{
    if(this.moving === e)
    {
        this.moving = null;
        this.moving_x = 0;
        this.moving_y = 0;
    }
}

Canvas.prototype.addObserver = function(o)
{
    this.observers.push(o);
}

Canvas.prototype.removeObserver = function(o)
{
    for(var i = 0; i < this.observers.length; i++)
        if(this.observers[i] === o)
        {
            this.observers.splice(i, 1);
            return;
        }
}

Canvas.prototype.notifyObservers = function()
{
    for(var i = 0; i < this.observers.length; i++)
        this.observers[i].call(this);
}

Canvas.prototype.dotClicked = function(dot)
{
    if(this.current_dot === null)
        this.current_dot = dot;
    else if(this.current_dot === dot)
        this.current_dot = null;
    else
    {
        new Line(this.current_dot, dot);
        this.current_dot = null;
    }
}

Canvas.prototype.resize = function()
{
    var width = 0;
    var height = 0;
    for(var i = 0; i < this.container.childNodes.length; i++)
    {
        var q = this.container.childNodes[i];
        if(q.className != 'line')
        {
            var w2 = q.offsetLeft + q.offsetWidth - this.container.offsetLeft;
            var h2 = q.offsetTop + q.offsetHeight - this.container.offsetTop;
            if(w2 > width)
                width = w2;
            if(h2 > height)
                height = h2;
        }
    }
    this.container.style.width = width+'px';
    this.container.style.height = height+'px';
}
