function Element(canvas, solver, x, y)
{
    this.elem = CLASS('element', TAG('div', []));
    this.canvas = canvas;
    this.solver = solver;
    this.canvas.appendChild(this.elem);
    this.dots = [];
    this.x = x;
    this.y = y;
    this.elem.style.left = this.x+'px';
    this.elem.style.top = this.y+'px';
    this.elem.onmousedown = function(event)
    {
        this.canvas.setMovingElement(this, this.x-event.pageX, this.y-event.pageY);
    }.bind(this);
    this.elem.oncontextmenu = function()
    {
        this.remove();
        return false;
    }.bind(this);
    requestAnimationFrame(this.canvas.resize.bind(this.canvas));
}

Element.prototype.move = function(x2, y2)
{
    var dx = x2 - this.x;
    var dy = y2 - this.y;
    for(var i = 0; i < this.dots.length; i++)
        this.dots[i].move(this.dots[i].x+dx, this.dots[i].y+dy);
    this.x = x2;
    this.y = y2;
    this.elem.style.left = this.x+'px';
    this.elem.style.top = this.y+'px';
    this.canvas.resize();
}

Element.prototype.remove = function()
{
    for(var i = 0; i < this.dots.length; i++)
        this.dots[i].remove();
    this.canvas.removeChild(this.elem);
    this.canvas.resetMovingElement(this);
}

function Element1to1(canvas, solver, x, y, inner, w)
{
    Element.call(this, canvas, solver, x, y);
    this.w = w;
    this.input = new Dot(canvas, solver, 'yellow');
    this.output = new Dot(canvas, solver, 'red');
    this.dots = [this.input, this.output];
    if(this.w !== null)
        this.solver.add_rule(this.input.id, this.w, this.output.id, P1.neg());
    this.elem.appendChild(inner);
    requestAnimationFrame(this.update_dots.bind(this));
}

Element1to1.prototype.__proto__ = Element.prototype;

Element1to1.prototype.set_w = function(w)
{
    if(this.w !== null)
        this.solver.remove_rule(this.input.id, this.w, this.output.id, P1.neg());
    this.w = w;
    if(this.w !== null)
        this.solver.add_rule(this.input.id, this.w, this.output.id, P1.neg());
    this.canvas.notifyObservers();
}

Element1to1.prototype.update_dots = function()
{
    var w = this.elem.offsetWidth;
    var h = this.elem.offsetHeight;
    this.input.move(this.x, this.y+h/2);
    this.output.move(this.x+w, this.y+h/2);
}

Element1to1.prototype.remove = function()
{
    if(this.w !== null)
        this.solver.remove_rule(this.input.id, this.w, this.output.id, P1.neg());
    Element.prototype.remove.call(this);
}

function Summator(canvas, solver, x, y)
{
    this.inner = TAG('div', [TEXT('+')]);
    this.is_neg = false;
    Element1to1.call(this, canvas, solver, x, y, this.inner, null);
    this.aux = new Dot(canvas, solver, 'blue');
    this.dots.push(this.aux);
    this.solver.add_rule(this.input.id, P1, this.aux.id, P1, this.output.id, P1.neg());
    this.inner.onmousedown = function()
    {
        this.inner_click = true;
    }.bind(this);
    this.inner.onmouseup = function()
    {
        if(this.inner_click)
        {
            this.inner_click = false;
            this.set_neg(!this.is_neg);
        }
    }.bind(this);
}

Summator.prototype.__proto__ = Element1to1.prototype;

Summator.prototype.move = function()
{
    Element1to1.prototype.move.apply(this, arguments);
    this.inner_click = false;
}

Summator.prototype.set_neg = function(neg)
{
    var PN = P1.neg();
    this.solver.remove_rule(this.input.id, P1, this.aux.id, this.is_neg?PN:P1, this.output.id, PN);
    this.is_neg = neg;
    this.solver.add_rule(this.input.id, P1, this.aux.id, this.is_neg?PN:P1, this.output.id, PN);
    this.inner.removeChild(this.inner.firstChild);
    this.inner.appendChild(TEXT(this.is_neg ? '\u2013' : '+'));
    this.canvas.notifyObservers();
}

Summator.prototype.update_dots = function()
{
    Element1to1.prototype.update_dots.call(this);
    var w = this.elem.offsetWidth;
    var h = this.elem.offsetHeight;
    this.aux.move(this.x+w/2, this.y+h);
}

Summator.prototype.remove = function()
{
    var PN = P1.neg();
    this.solver.remove_rule(this.input.id, P1, this.aux.id, this.is_neg?PN:P1, this.output.id, PN);
    Element1to1.prototype.remove.call(this);
}

function Element1(canvas, solver, x, y, wp, color)
{
    Element.call(this, canvas, solver, x, y);
    this.wp = wp;
    this.dot = new Dot(canvas, solver, color);
    this.dots = [this.dot];
    requestAnimationFrame(this.update_dots.bind(this));
}

Element1.prototype.__proto__ = Element.prototype;

Element1.prototype.update_dots = function()
{
    var w = this.elem.offsetWidth;
    var h = this.elem.offsetHeight;
    this.dot.move(this.x+w*this.wp, this.y+h/2);
}

function JustDot(canvas, solver, x, y)
{
    Element1.call(this, canvas, solver, x, y, 0.5, 'white');
    this.elem.className += ' justdot';
}

JustDot.prototype.__proto__ = Element1.prototype;
