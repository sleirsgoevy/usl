function TAG(tag, items, css)
{
    var ans = document.createElement(tag);
    for(var i = 0; i < items.length; i++)
        ans.appendChild(items[i]);
    if(css !== undefined)
        for(var i in css)
            ans.style[i] = css[i];
    return ans;
}

function TEXT(t)
{
    return document.createTextNode(t);
}

function CLASS(what, tag)
{
    tag.className = what;
    return tag;
}

function poly2html(x)
{
    if(x instanceof PolyFrac)
    {
        if(x.b.sub(P1).iszero())
            return poly2html(x.a);
        return [TAG('table', [
            TAG('tr', [TAG('td', [TAG('nobr', poly2html(x.a))], {borderBottom: '1px solid black'})]),
            TAG('tr', [TAG('td', [TAG('nobr', poly2html(x.b))])])
        ])];
    }
    else if(x instanceof Polynomial)
    {
        if(!x.coefs.length)
            return [TEXT('0')];
        var ans = [];
        var first = true;
        for(var i = x.coefs.length - 1; i >= 0; i--)
        {
            if(x.coefs[i].iszero())
                continue;
            if(!first)
                ans.push(TEXT(' '));
            var s = null;
            if(x.coefs[i].isneg())
            {
                ans.push(TEXT('\u2013'));
                if(!first)
                    ans.push(TEXT(' '));
                if(!i || !x.coefs[i].add(F1).iszero())
                {
                    var q = poly2html(x.coefs[i].neg());
                    for(var j = 0; j < q.length; j++)
                        ans.push(q[j]);
                }
            }
            else
            {
                if(!first)
                    ans.push(TEXT('+ '));
                if(!i || !x.coefs[i].sub(F1).iszero())
                {
                    var q = poly2html(x.coefs[i]);
                    for(var j = 0; j < q.length; j++)
                        ans.push(q[j]);
                }
            }
            if(i)
            {
                ans.push(TAG('i', [TEXT('s')]));
                if(i > 1)
                    ans.push(TAG('sup', [TEXT(''+i)]));
            }
            first = false;
        }
        return ans;
    }
    else if(x instanceof Fraction)
    {
        if(x.b.sub(1).iszero())
            return poly2html(x.a);
        return [TAG('table', [
            TAG('tr', [TAG('td', [TAG('nobr', poly2html(x.a))], {borderBottom: '1px solid black'})]),
            TAG('tr', [TAG('td', [TAG('nobr', poly2html(x.b))])])
        ])];
    }
    else
        return [TEXT(''+x)];
}
