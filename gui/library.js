function createIntegrator(canvas, solver)
{
    new Element1to1(
        canvas,
        solver,
        canvas.last_x += 10,
        canvas.last_y += 10,
        TEXT('\u222b'),
        P1.div(S)
    );
    canvas.notifyObservers();
}

function createDifferentiator(canvas, solver)
{
    new Element1to1(
        canvas,
        solver,
        canvas.last_x += 10,
        canvas.last_y += 10,
        CLASS('formula', TAG('table', [
            TAG('tr', [TAG('td', [TEXT('d')], {borderBottom: '1px solid black'})]),
            TAG('tr', [TAG('td', [TAG('span', [TEXT('d'), TAG('i', [TEXT('t')])])])]),
        ])),
        S
    );
    canvas.notifyObservers();
}

function createWMeter(canvas, solver)
{
    new WMeter(canvas, solver, canvas.last_x += 10, canvas.last_y += 10);
}

function createDot(canvas, solver)
{
    new JustDot(canvas, solver, canvas.last_x += 10, canvas.last_y += 10);
}

function createSummator(canvas, solver)
{
    new Summator(canvas, solver, canvas.last_x += 10, canvas.last_y += 10);
}

function createMultiplier0(canvas, solver, parser)
{
    var input = TAG('input', []);
    var ans = new Element1to1(
        canvas,
        solver,
        canvas.last_x += 10,
        canvas.last_y += 10,
        input,
        null
    );
    input.onchange = function()
    {
        try
        {
            ans.set_w(parser(input.value));
        }
        catch(e)
        {
            alert(e);
        }
    }
}

function parse_fraction(s)
{
    if(s.indexOf('/') >= 0)
    {
        var q = s.split('/');
        var a = new BigNum(q[0].trim());
        var b = new BigNum(q[1].trim());
        return new Fraction(a, b);
    }
    else
        return new BigNum(s.trim());
}

function createMultiplier(canvas, solver)
{
    return createMultiplier0(canvas, solver, parse_fraction);
}

function createW(canvas, solver)
{
    return createMultiplier0(canvas, solver, parse_expr);
}
