function WMeter(canvas, solver, x, y)
{
    this.output_field = TAG('td', []);
    Element1to1.call(
        this,
        canvas,
        solver,
        x,
        y,
        CLASS('formula', TAG('table', [
            TAG('tr', [
                TAG('td', [TAG('i', [TEXT('W')])]),
                TAG('td', [TEXT('=')]),
                this.output_field
            ])
        ])),
        null
    );
    this.handler = this.recalculate.bind(this);
    this.canvas.addObserver(this.handler);
    this.recalculate();
}

WMeter.prototype.__proto__ = Element1to1.prototype;

WMeter.prototype.recalculate = function()
{
    try
    {
        var w = this.solver.get_w(this.input.id, this.output.id);
        var disp = poly2html(w);
    }
    catch(e)
    {
        disp = [TEXT('Error: '+e)];
    }
    while(this.output_field.firstChild)
        this.output_field.removeChild(this.output_field.firstChild);
    for(var i = 0; i < disp.length; i++)
        this.output_field.appendChild(disp[i]);
    requestAnimationFrame(this.update_dots.bind(this));
}

WMeter.prototype.remove = function()
{
    this.canvas.removeObserver(this.handler);
    Element1to1.prototype.remove.call(this);
}
