function Dot(canvas, solver, color)
{
    this.elem = CLASS('dot', TAG('div', [], {background: color}));
    this.canvas = canvas;
    this.canvas.appendChild(this.elem);
    this.solver = solver;
    this.id = this.solver.alloc();
    this.onmoves = [];
    this.elem.onclick = this.canvas.dotClicked.bind(this.canvas, this);
}

Dot.prototype.addOnmove = function(f)
{
    this.onmoves.push(f);
}

Dot.prototype.removeOnmove = function(f)
{
    for(var i = 0; i < this.onmoves.length; i++)
        if(this.onmoves[i] === f)
        {
            this.onmoves.splice(i, 1);
            return;
        }
}

Dot.prototype.move = function(x, y)
{
    this.x = x;
    this.y = y;
    this.elem.style.left = x+'px';
    this.elem.style.top = y+'px';
    for(var i = 0; i < this.onmoves.length; i++)
        this.onmoves[i].call(this, x, y);
}

Dot.prototype.remove = function()
{
    canvas.removeChild(this.elem);
    while(this.onmoves.length)
        this.onmoves[0].call(this, null, null);
    this.solver.free(this.id);
}

function create_line()
{
    return CLASS('line', TAG('table', [
        TAG('tr', [
            CLASS('linebegend', TAG('th', [])),
            TAG('th', []),
            CLASS('linebegend', TAG('th', []))
        ]),
        TAG('tr', [
            CLASS('linebegend', TAG('td', [])),
            TAG('td', []),
            CLASS('linebegend', TAG('td', []))
        ])
    ]));
}

function Line(a, b)
{
    this.a = a;
    this.b = b;
    this.canvas = this.a.canvas;
    this.solver = this.a.solver;
    this.solver.add_rule(this.a.id, P1, this.b.id, P1.neg());
    this.elem = create_line();
    this.handler = this.update_pos.bind(this);
    this.a.addOnmove(this.handler);
    this.b.addOnmove(this.handler);
    this.update_pos();
    this.elem.onclick = this.remove.bind(this);
    this.canvas.appendChild(this.elem);
    this.canvas.notifyObservers();
}

Line.prototype.update_pos = function(x, y)
{
    if(x === null && y === null)
    {
        this.remove();
        return;
    }
    var x1 = this.a.x;
    var y1 = this.a.y;
    var x2 = this.b.x;
    var y2 = this.b.y;
    this.elem.style.left = x1+'px';
    this.elem.style.top = y1+'px';
    this.elem.style.width = Math.hypot(x2-x1, y2-y1)+'px';
    this.elem.style.transform = 'rotate('+Math.atan2(y2-y1, x2-x1)+'rad)';
}

Line.prototype.remove = function()
{
    this.canvas.removeChild(this.elem);
    this.a.removeOnmove(this.handler);
    this.b.removeOnmove(this.handler);
    this.solver.remove_rule(this.a.id, P1, this.b.id, P1.neg());
    this.canvas.notifyObservers();
}
