function BigNum(num)
{
    if((typeof num) == "number")
    {
        this.data = [];
        while(num != (num | 0))
        {
            var cur = ((num | 0) + 0x100000000) % 0x100000000;
            this.data.push(cur);
            num = (num - cur) / 0x100000000;
        }
        this.data.push(num);
    }
    else if(num instanceof Array)
    {
        this.data = num.slice(0);
        while(this.data.length > 1 && this.data[this.data.length-1] == 0 && this.data[this.data.length-2] < 0x80000000)
            this.data.pop();
        while(this.data.length > 1 && this.data[this.data.length-1] == -1 && this.data[this.data.length-2] == 0xffffffff)
        {
            this.data.pop();
            this.data[this.data.length-1] = -1;
        }
    }
    else
    {
        num = '' + num;
        var ans = new BigNum(0);
        for(var i = 0; i < num.length; i++)
            if(num[i] >= '0' && num[i] <= '9')
                ans = ans.mul_small(10).add(+num[i]);
            else
                throw new Error("invalid number "+num);
        return ans;
    }
    this.validate();
}

BigNum.prototype.validate = function()
{
    if(!this.data.length)
        throw new Error("invalid bignum");
    for(var i = 0; i + 1 < this.data.length; i++)
        if(this.data[i] < 0 || this.data[i] >= 0x100000000)
            throw new Error("invalid bignum");
    if(this.data[this.data.length-1] != (this.data[this.data.length-1] | 0))
        throw new Error("invalid bignum");
}

BigNum.prototype.add = function(other)
{
    if(other.radd_BigNum)
        return other.radd_BigNum(this);
    if(!(other instanceof BigNum))
        other = new BigNum(other);
    var data1 = this.data.slice(0);
    var data2 = other.data.slice(0);
    while(data1.length < data2.length)
    {
        data1.push(data1[data1.length-1] >> 31);
        data1[data1.length-2] = (data1[data1.length-2] + 0x100000000) % 0x100000000;
    }
    while(data2.length < data1.length)
    {
        data2.push(data2[data2.length-1] >> 31);
        data2[data2.length-2] = (data2[data2.length-2] + 0x100000000) % 0x100000000;
    }
    var ans = [];
    var carry = 0;
    for(var i = 0; i < data1.length; i++)
    {
        var q = data1[i] + data2[i] + carry;
        var cur = (q % 0x100000000 + 0x100000000) % 0x100000000;
        ans.push(cur);
        carry = (q - cur) / 0x100000000;
    }
    if(carry || ans[ans.length-1] >= 0x80000000)
        ans.push(carry);
    return new BigNum(ans);
}

BigNum.prototype.neg = function()
{
    var ans = [];
    var carry = 0;
    for(var i = 0; i < this.data.length; i++)
    {
        var q = carry - this.data[i];
        var cur = (q % 0x100000000 + 0x100000000) % 0x100000000;
        ans.push(cur);
        carry = (q - cur) / 0x100000000;
    }
    if(carry || ans[ans.length-1] >= 0x80000000)
        ans.push(carry);
    return new BigNum(ans);
}

BigNum.prototype.sub = function(other)
{
    if(other.rsub_BigNum)
        return other.rsub_BigNum(this);
    if(!(other instanceof BigNum))
        other = new BigNum(other);
    return this.add(other.neg());
}

BigNum.prototype.mul_small = function(other)
{
    var ans = [];
    var carry = 0;
    for(var i = 0; i < this.data.length; i++)
    {
        var q = carry + this.data[i] * other;
        var cur = (q % 0x100000000 + 0x100000000) % 0x100000000;
        ans.push(cur);
        carry = (q - cur) / 0x100000000;
    }
    if(carry || ans[ans.length-1] >= 0x80000000)
        ans.push(carry);
    return new BigNum(ans);
}

BigNum.prototype.mul32 = function(other)
{
    var low = other % 0x10000;
    var high = (other - low) / 0x10000;
    return this.mul_small(low).add(this.mul_small(high).mul_small(0x10000));
}

BigNum.prototype.mul = function(other)
{
    if(other.rmul_BigNum)
        return other.rmul_BigNum(this);
    if(!(other instanceof BigNum))
        other = new BigNum(other);
    var ans = new BigNum(0);
    for(var i = 0; i < other.data.length; i++)
    {
        var cur = this.mul32(other.data[i]);
        var q = [];
        for(var j = 0; j < i; j++)
            q.push(0);
        for(var j = 0; j < cur.data.length; j++)
            q.push(cur.data[j]);
        ans = ans.add(new BigNum(q));
    }
    return ans;
}

BigNum.prototype.divmod = function(other)
{
    var self = this;
    if(!(other instanceof BigNum))
        other = new BigNum(other);
    if(other.iszero())
        throw new Error("division by zero");
    if(other.data[other.data.length-1] < 0)
    {
        var dm2 = self.neg().divmod(other.neg());
        dm2[1] = dm2[1].neg();
        return dm2;
    }
    if(self.data[self.data.length-1] < 0)
    {
        var dm2 = self.neg().divmod(other);
        dm2[0] = dm2[0].neg();
        if(dm2[1].data.length != 1 || dm2[1].data[0])
        {
            dm2[0] = dm2[0].sub(1);
            dm2[1] = other.sub(dm2[1]);
        }
        return dm2;
    }
    var ans = new BigNum(0);
    //here both numbers are known to be positive
    for(var i = this.data.length - other.data.length; i >= 0; i--)
    {
        var qq = [];
        for(var j = 0; j < i; j++)
            qq.push(0);
        for(var j = 0; j < other.data.length; j++)
            qq.push(other.data[j]);
        qq = new BigNum(qq);
        for(var m = 0x80000000; m >= 1; m = (m - m % 2) / 2)
        {
            var qqq = qq.mul32(m);
            var s2 = self.sub(qqq);
            if(s2.data[s2.data.length-1] >= 0)
            {
                self = s2;
                var qqqq = [];
                for(var j = 0; j < i; j++)
                    qqqq.push(0);
                qqqq.push(m);
                qqqq.push(0);
                ans = ans.add(new BigNum(qqqq));
            }
        }
    }
    return [ans, self];
}

BigNum.prototype.div = function(other)
{
    return other.rdiv_BigNum(this);
}

BigNum.prototype.isneg = function()
{
    return this.data[this.data.length - 1] < 0;
}

BigNum.prototype.iszero = function()
{
    return this.data.length == 1 && this.data[0] == 0;
}

BigNum.prototype.toString = function()
{
    if(this.iszero())
        return 0;
    if(this.isneg())
        return '-' + this.neg();
    var s = "";
    var self = this;
    while(!self.iszero())
    {
        var q = self.divmod(10);
        s = q[1].data[0] + s;
        self = q[0];
    }
    return s;
}

BigNum.prototype.rdiv_BigNum = function(other)
{
    return new Fraction(other, this);
}
