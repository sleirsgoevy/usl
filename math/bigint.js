function BigIntWrapper(num)
{
    this.value = BigInt(num);
}

BigIntWrapper.prototype.add = function(other)
{
    if(other.radd_BigNum)
        return other.radd_BigNum(this);
    if(!(other instanceof BigIntWrapper))
        other = new BigIntWrapper(other);
    return new BigIntWrapper(this.value + other.value);
}

BigIntWrapper.prototype.neg = function()
{
    return new BigIntWrapper(-this.value);
}

BigIntWrapper.prototype.sub = function(other)
{
    if(other.rsub_BigNum)
        return other.rsub_BigNum(this);
    if(!(other instanceof BigIntWrapper))
        other = new BigIntWrapper(other);
    return new BigIntWrapper(this.value - other.value);
}

BigIntWrapper.prototype.mul = function(other)
{
    if(other.rmul_BigNum)
        return other.rmul_BigNum(this);
    if(!(other instanceof BigIntWrapper))
        other = new BigIntWrapper(other);
    return new BigIntWrapper(this.value * other.value);
}

BigIntWrapper.prototype.divmod = function(other)
{
    if(other.rmul_BigNum)
        return other.rmul_BigNum(this);
    if(!(other instanceof BigIntWrapper))
        other = new BigIntWrapper(other);
    if(other.iszero())
        throw new Error("division by zero");
    var m = this.value % other.value;
    if(m * other.value < 0)
        m += other.value;
    var d = (this.value - m) / other.value;
    return [new BigIntWrapper(d), new BigIntWrapper(m)];
}

BigIntWrapper.prototype.div = function(other)
{
    return other.rdiv_BigNum(this);
}

BigIntWrapper.prototype.isneg = function()
{
    return this.value < 0;
}

BigIntWrapper.prototype.iszero = function()
{
    return this.value == 0;
}

BigIntWrapper.prototype.toString = function()
{
    return this.value.toString();
}

BigIntWrapper.prototype.rdiv_BigNum = function(other)
{
    return new Fraction(other, this);
}

try
{
    BigInt;
    BigNum = BigIntWrapper;
}
catch(e){}
