function gcd_rows(r1, r2, i, n)
{
    var a = r1[i];
    var b = r2[i];
    var aa = P1;
    var ab = P0;
    var ba = P0;
    var bb = P1;
    while(!a.iszero())
    {
        var c = a;
        a = b;
        b = c;
        c = aa;
        aa = ba;
        ba = c;
        c = ab;
        ab = bb;
        bb = c;
        var dm = a.divmod(b);
        aa = aa.sub(ba.mul(dm[0]));
        ab = ab.sub(bb.mul(dm[0]));
        a = dm[1];
    }
    for(var j = 0; j < n; j++)
    {
        var upper = ba.mul(r1[j]).add(bb.mul(r2[j]));
        var lower = aa.mul(r1[j]).add(ab.mul(r2[j]));
        r1[j] = upper;
        r2[j] = lower;
    }
}

function solve(matrix0, n)
{
    var matrix = [];
    for(var i = 0; i < matrix0.length; i++)
        matrix.push(matrix0[i].slice(0));
    var I = [];
    for(var i = 0; i < n; i++)
        I.push(i);
    var m = matrix.length;
    for(var row = 0; row < m && row < n; row++)
    {
        var y = -1;
        var x = -1;
        var found = false;
        for(var j = row; j < n && !found; j++)
            for(var i = row; i < m && !found; i++)
                if(!matrix[i][I[j]].iszero())
                {
                    y = i;
                    x = j;
                    found = true;
                }
        if(!found)
        {
            m = row;
            break;
        }
        var z = I[x];
        I[x] = I[row];
        I[row] = z;
        var zz = matrix[y];
        matrix[y] = matrix[row];
        matrix[row] = zz;
        for(var j = row+1; j < m; j++)
            gcd_rows(matrix[row], matrix[j], I[row], n);
    }
    var ans = [];
    for(var i = m; i < n; i++)
    {
        var q = new Array(n);
        for(var j = m; j < n; j++)
            q[I[j]] = P0;
        q[I[i]] = P1;
        for(var j = m-1; j >= 0; j--)
        {
            var sum = P0;
            for(var k = j+1; k < n; k++)
                sum = sum.add(matrix[j][I[k]].mul(q[I[k]]));
            q[I[j]] = sum.neg().div(matrix[j][I[j]]);
        }
        ans.push(q);
    }
    return ans;
}

function Solver()
{
    this.n = 0;
    this.matrix = [];
    this.freelist = [];
}

Solver.prototype.alloc = function()
{
    if(this.freelist.length)
        return this.freelist.pop();
    var ans = this.n++;
    for(var i = 0; i < this.matrix.length; i++)
        this.matrix[i].push(F0);
    this.solutions = null;
    return ans;
}

Solver.prototype.free = function(idx)
{
    this.freelist.push(idx);
}

Solver.prototype.make_rule = function()
{
    var q = [];
    for(var i = 0; i < this.n; i++)
        q.push(new PolyFrac(P0));
    for(var i = 0; i < arguments.length; i += 2)
    {
        var v = arguments[i+1];
        if(!(v instanceof PolyFrac))
            v = new PolyFrac(v);
        q[arguments[i]] = v;
    }
    var common_modulo = P1;
    for(var i = 0; i < q.length; i++)
        common_modulo = common_modulo.mul(q[i].b).divmod(gcd(common_modulo, q[i].b))[0];
    for(var i = 0; i < q.length; i++)
        q[i] = q[i].mul(common_modulo).a;
    return q;
}

Solver.prototype.add_rule = function()
{   
    this.matrix.push(this.make_rule.apply(this, arguments));
    this.solutions = null;
}

Solver.prototype.remove_rule = function()
{
    var r = this.make_rule.apply(this, arguments);
    for(var i = 0; i < this.matrix.length; i++)
    {
        var equal = true;
        for(var j = 0; j < this.n && equal; j++)
            if(!this.matrix[i][j].sub(r[j]).iszero())
                equal = false;
        if(equal)
        {
            this.matrix.splice(i, 1);
            break;
        }
    }
    this.solutions = null;
}

Solver.prototype.solve = function()
{
    if(this.solutions !== null)
        return;
    var sols = solve(this.matrix, this.n);
    /*if(!sols.length)
        throw new Error("Impossible dynamic system!");*/
    this.solutions = sols;
}

Solver.prototype.get_w = function(x, y)
{
    this.solve();
    var ans = null;
    for(var i = 0; i < this.solutions.length; i++)
    {
        var q = this.solutions[i][y].div(this.solutions[i][x]);
        if(ans === null)
        {
            if(!q.a.iszero() || !q.b.iszero())
                ans = q;
        }
        else
        {
            if(!q.sub(ans).iszero())
                return "Ambiguous";
        }
    }
    if(ans === null)
        return "Impossible";
    if(!ans.a.iszero() && ans.b.iszero())
        return "Infinity";
    return ans;
}
