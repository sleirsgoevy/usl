function Polynomial(coefs)
{
    if(coefs instanceof Polynomial)
        return coefs;
    if(!(coefs instanceof Array))
        coefs = [coefs];
    else
        coefs = coefs.slice(0);
    for(var i = 0; i < coefs.length; i++)
        if(!(coefs[i] instanceof Fraction))
            coefs[i] = new Fraction(coefs[i]);
    this.coefs = coefs;
    while(this.coefs.length && this.coefs[this.coefs.length-1] == 0)
        this.coefs.pop();
}

Polynomial.prototype.add = function(other)
{
    if(other.radd_Polynomial)
        return other.radd_Polynomial(this);
    if(!(other instanceof Polynomial))
        other = new Polynomial(other);
    var c1 = this.coefs.slice(0);
    var c2 = other.coefs.slice(0);
    while(c1.length < c2.length)
        c1.push(F0);
    while(c2.length < c1.length)
        c2.push(F0);
    var q = [];
    for(var i = 0; i < c1.length; i++)  
        q.push(c1[i].add(c2[i]));
    return new Polynomial(q);
}

Polynomial.prototype.radd_BigNum = Polynomial.prototype.add;
Polynomial.prototype.radd_Fraction = Polynomial.prototype.add;

Polynomial.prototype.neg = function()
{
    var q = [];
    for(var i = 0; i < this.coefs.length; i++)
        q.push(this.coefs[i].neg());
    return new Polynomial(q);
}

Polynomial.prototype.sub = function(other)
{
    if(other.rsub_Polynomial)
        return other.rsub_Polynomial(this);
    if(!(other instanceof Polynomial))
        other = new Polynomial(other);
    return this.add(other.neg());
}

Polynomial.prototype.rsub_BigNum = function(other)
{
    return this.neg().add(other);
}

Polynomial.prototype.rsub_Fraction = Polynomial.prototype.rsub_BigNum;

Polynomial.prototype.mul = function(other)
{
    if(other.rmul_Polynomial)
        return other.rmul_Polynomial(this);
    if(!(other instanceof Polynomial))
        other = new Polynomial(other);
    var ans = new Polynomial(0);
    for(var i = 0; i < this.coefs.length; i++)
    {
        if(this.coefs[i].iszero())
            continue;
        var q = [];
        for(var j = 0; j < i; j++)
            q.push(F0);
        for(var j = 0; j < other.coefs.length; j++)
            q.push(this.coefs[i].mul(other.coefs[j]));
        ans = ans.add(new Polynomial(q));
    }
    return ans;
}

Polynomial.prototype.rmul_BigNum = Polynomial.prototype.mul;
Polynomial.prototype.rmul_Fraction = Polynomial.prototype.mul;

Polynomial.prototype.divmod = function(other)
{
    if(other.rdivmod_Polynomial)
        return other.rdivmod_Polynomial(this);
    if(!(other instanceof Polynomial))
        return P0;
    var self = this;
    var ans = P0;
    while(self.coefs.length >= other.coefs.length)
    {
        var q = self.coefs[self.coefs.length-1].div(other.coefs[other.coefs.length-1]);
        var qq = [];
        for(var i = other.coefs.length; i < self.coefs.length; i++)
            qq.push(F0);
        for(var i = 0; i < other.coefs.length; i++)
            qq.push(other.coefs[i].mul(q));
        var qqq = [];
        for(var i = other.coefs.length; i < self.coefs.length; i++)
            qqq.push(F0);
        qqq.push(q);
        ans = ans.add(new Polynomial(qqq));
        self = self.sub(new Polynomial(qq));
    }
    return [ans, self];
}

Polynomial.prototype.div = function(other)
{
    if(other.rdiv_Polynomial)
        return other.rdiv_Polynomial(this);
    return new PolyFrac(this, other);
}

Polynomial.prototype.rdiv_BigNum = function(other)
{
    return new PolyFrac(other, this);
}

Polynomial.prototype.rdiv_Fraction = Polynomial.prototype.rdiv_BigNum;

Polynomial.prototype.iszero = function()
{
    return !this.coefs.length;
}

Polynomial.prototype.toString = function()
{
    if(!this.coefs.length)
        return '0';
    var q = [];
    for(var i = this.coefs.length-1; i >= 0; i--)
        if(!this.coefs[i].iszero())
            q.push(this.coefs[i]+'*s**'+i);
    return q.join(' + ').split(' + -').join(' - ');
}

function PolyFrac(a, b)
{
    if(b === undefined)
        b = P1;
    if(!(a instanceof Polynomial))
        a = new Polynomial(a);
    if(!(b instanceof Polynomial))
        b = new Polynomial(b);
    var g = gcd(a, b);
    if(g.iszero())
    {
        this.a = a;
        this.b = b;
    }
    else
    {
        this.a = a.divmod(g)[0];
        this.b = b.divmod(g)[0];
        if(!this.b.iszero())
        {
            var q = F1.div(this.b.coefs[this.b.coefs.length-1]);
            if(!q.sub(1).iszero())
            {
                this.a = this.a.mul(q);
                this.b = this.b.mul(q);
            }
        }
    }
}

PolyFrac.prototype.add = function(other)
{
    if(other.radd_PolyFrac)
        return other.radd_PolyFrac(this);
    if(!(other instanceof PolyFrac))
        other = new PolyFrac(other);
    return new PolyFrac(this.a.mul(other.b).add(this.b.mul(other.a)), this.b.mul(other.b));
}

PolyFrac.prototype.radd_BigNum = PolyFrac.prototype.add;
PolyFrac.prototype.radd_Fraction = PolyFrac.prototype.add;
PolyFrac.prototype.radd_Polynomial = PolyFrac.prototype.add;

PolyFrac.prototype.neg = function(other)
{
    return new PolyFrac(this.a.neg(), this.b);
}

PolyFrac.prototype.sub = function(other)
{
    if(other.rsub_PolyFrac)
        return other.rsub_PolyFrac(this);
    if(!(other instanceof PolyFrac))
        other = new PolyFrac(other);
    return this.add(other.neg());
}

PolyFrac.prototype.rsub_BigNum = function(other)
{
    return this.neg().add(other);
}

PolyFrac.prototype.rsub_Fraction = PolyFrac.prototype.rsub_BigNum;
PolyFrac.prototype.rsub_Polynomial = PolyFrac.prototype.rsub_BigNum;

PolyFrac.prototype.mul = function(other)
{
    if(other.rmul_PolyFrac)
        return other.rmul_PolyFrac(this);
    if(!(other instanceof PolyFrac))
        other = new PolyFrac(other);
    return new PolyFrac(this.a.mul(other.a), this.b.mul(other.b));
}

PolyFrac.prototype.rmul_BigNum = PolyFrac.prototype.mul;
PolyFrac.prototype.rmul_Fraction = PolyFrac.prototype.mul;
PolyFrac.prototype.rmul_Polynomial = PolyFrac.prototype.mul;

PolyFrac.prototype.div = function(other)
{
    if(other.rdiv_PolyFrac)
        return other.rdiv_PolyFrac(this);
    if(!(other instanceof PolyFrac))
        other = new PolyFrac(other);
    return new PolyFrac(this.a.mul(other.b), this.b.mul(other.a));
}

PolyFrac.prototype.rdiv_BigNum = function(other)
{
    return (new PolyFrac(other)).div(this);
}

PolyFrac.prototype.rdiv_Fraction = PolyFrac.prototype.rdiv_BigNum;
PolyFrac.prototype.rdiv_Polynomial = PolyFrac.prototype.rdiv_BigNum;

PolyFrac.prototype.divmod = function(other)
{
    return other.rdivmod_PolyFrac(this);
}

PolyFrac.prototype.iszero = function()
{
    return this.a.iszero();
}

PolyFrac.prototype.toString = function()
{
    if(this.b.sub(1).iszero())
        return ''+this.a;
    return '('+this.a+')/('+this.b+')';
}

P0 = new Polynomial([]);
P1 = new Polynomial([1]);
S = new Polynomial([0, 1]);
