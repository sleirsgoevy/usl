function gcd(a, b)
{
    while(!a.iszero())
    {
        var c = b;
        b = a;
        a = c.divmod(b)[1];
    }
    return b;
}

function Fraction(a, b)
{
    if(b === undefined)
        b = 1;
    if(!(a instanceof BigNum))
        a = new BigNum(a);
    if(!(b instanceof BigNum))
        b = new BigNum(b);
    var g = gcd(a, b);
    if(g.iszero())
    {
        this.a = a;
        this.b = b;
    }
    else
    {
        this.a = a.divmod(g)[0];
        this.b = b.divmod(g)[0];
    }
    if(this.b.isneg())
    {
        this.a = this.a.neg();
        this.b = this.b.neg();
    }
}

Fraction.prototype.add = function(other)
{
    if(other.radd_Fraction)
        return other.radd_Fraction(this);
    if(!(other instanceof Fraction))
        other = new Fraction(other);
    return new Fraction(this.a.mul(other.b).add(this.b.mul(other.a)), this.b.mul(other.b));
}

Fraction.prototype.radd_BigNum = Fraction.prototype.add;

Fraction.prototype.neg = function()
{
    return new Fraction(this.a.neg(), this.b);
}

Fraction.prototype.sub = function(other)
{
    if(other.rsub_Fraction)
        return other.rsub_Fraction(this);
    if(!(other instanceof Fraction))
        other = new Fraction(other);
    return this.add(other.neg());
}

Fraction.prototype.rsub_BigNum = function(other)
{
    return this.neg().add(other);
}

Fraction.prototype.mul = function(other)
{
    if(other.rmul_Fraction)
        return other.rmul_Fraction(this);
    if(!(other instanceof Fraction))
        other = new Fraction(other);
    return new Fraction(this.a.mul(other.a), this.b.mul(other.b));
}

Fraction.prototype.rmul_BigNum = Fraction.prototype.mul;

Fraction.prototype.div = function(other)
{
    if(other.rdiv_Fraction)
        return other.rdiv_Fraction(this);
    if(!(other instanceof Fraction))
        other = new Fraction(other);
    return new Fraction(this.a.mul(other.b), this.b.mul(other.a));
}

Fraction.prototype.rdiv_BigNum = function(other)
{
    return (new Fraction(other)).div(this);
}

Fraction.prototype.divmod = function(other)
{
    return other.rdivmod_Fraction(this);
}

Fraction.prototype.isneg = function()
{
    return this.a.isneg();
}

Fraction.prototype.iszero = function()
{
    return this.a.iszero();
}

Fraction.prototype.toString = function()
{
    if(this.b.sub(1).iszero())
        return ''+this.a;
    return this.a+'/'+this.b;
}

F0 = new Fraction(0);
F1 = new Fraction(1);
