function Stream(s)
{
    this.s = s;
    this.i = 0;
}

Stream.prototype.get = function()
{
    if(this.i == this.s.length)
    {
        this.i++;
        return '';
    }
    return this.s[this.i++];
}

Stream.prototype.unget = function()
{
    this.i--;
}

function fast_pow(q, n)
{
    if(n.isneg())
        return F1.div(fast_pow(q, n.neg()));
    else if(n.iszero())
        return new BigNum(1);
    var dm = n.divmod(2);
    if(!dm[1].iszero())
        return fast_pow(q, n.sub(1)).mul(q);
    else
        return fast_pow(q.mul(q), dm[0]);
}

function isspace(c)
{
    return c == ' ' || c == '\r' || c == '\n' || c == '\t';
}

function parse_solid(s)
{
    var c = s.get();
    while(isspace(c))
        c = s.get();
    if(c == '(')
    {
        var ans = parse_addsub(s);
        c = s.get();
        while(isspace(c))
            c = s.get();
        if(c != ')')
            throw new Error("Expected ), got "+c);
        return ans;
    }
    if(c == 's')
        return S;
    var num = new BigNum(0);
    var any = c >= '0' && c <= '9';
    while(c >= '0' && c <= '9')
    {
        num = num.mul(10).add(c);
        c = s.get();
    }
    if(c == '.')
    {
        c = s.get();
        if(c >= '0' && c <= '9')
            any = any || (c >= '0' && c <= '9');
        var div = new BigNum(1);
        while(c >= '0' && c <= '9')
        {
            num = num.mul(10).add(c);
            div = div.mul(10);
            c = s.get();
        }
        num = num.div(div);
    }
    if(!any)
        throw new Error("Expected a number, got "+c);
    if(c == 'e')
    {
        var p = new BigNum(0);
        c = s.get();
        if(c < '0' || c > '9')
            throw new Error("Expected a power, got "+c);
        while(c >= '0' && c <= '9')
        {
            p = p.mul(10).add(c);
            c = s.get();
        }
        num = num.mul(fast_pow(new BigNum(10), p));
    }
    s.unget();
    return num;
}

function parse_unary(s)
{
    var c = s.get();
    while(isspace(c))
        c = s.get();
    if(c == '+' || c == '-')
    {
        var q = parse_unary(s);
        if(c == '+')
            return q;
        if(c == '-')
            return q.neg();
    }
    s.unget();
    return parse_solid(s);
}

function parse_pow(s)
{
    var q = parse_unary(s);
    var c = s.get();
    while(isspace(c))
        c = s.get();
    if(c == '*')
    {
        var c2 = s.get();
        if(c2 != '*')
        {
            s.unget();
            s.unget();
            return q;
        }
        c = '^';
    }
    if(c == '^')
    {
        var qq = parse_pow(s);
        if(!(qq instanceof BigNum))
            throw new Error("Power must be a whole number");
        return fast_pow(q, qq);
    }
    s.unget();
    return q;
}

function parse_muldiv(s)
{
    var q = parse_pow(s);
    var c = s.get();
    while(isspace(c))
        c = s.get();
    while(c == '*' || c == '/')
    {
        var qq = parse_solid(s);
        if(c == '*')
            q = q.mul(qq);
        else if(c == '/')
            q = q.div(qq);
        c = s.get();
        while(isspace(c))
            c = s.get();
    }
    s.unget();
    return q;
}

function parse_addsub(s)
{
    var q = parse_muldiv(s);
    var c = s.get();
    while(isspace(c))
        c = s.get();
    while(c == '+' || c == '-')
    {
        var qq = parse_muldiv(s);
        if(c == '+')
            q = q.add(qq);
        else if(c == '-')
            q = q.sub(qq);
        c = s.get();
        while(isspace(c))
            c = s.get();
    }
    s.unget();
    return q;
}

function parse_expr(s)
{
    var ss = new Stream(s);
    var ans = parse_addsub(ss);
    var c = ss.get();
    while(isspace(c))
        c = ss.get();
    if(c != '')
        throw new Error("Unexpected "+c);
    return ans;
}
